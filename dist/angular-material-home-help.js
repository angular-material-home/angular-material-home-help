/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeHelp', [
	'ngMaterialHome',
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//angular.module('ngMaterialHomeHelp')
///**
// * State of the user management
// */
//.config(function($routeProvider) {
//	$routeProvider//
//	.when('/users/account', {
//		templateUrl : 'views/amh-account.html',
//		controller : 'MbAccountCtrl'
//	})//
//	.when('/user/account', {
//		templateUrl : 'views/amh-account.html',
//		controller : 'MbAccountCtrl'
//	})//
//	.when('/users/profile', {
//		templateUrl : 'views/amh-profile.html',
//		controller : 'AmhProfileCtrl'
//	})//
//	.when('/user/profile', {
//		templateUrl : 'views/amh-profile.html',
//		controller : 'AmhProfileCtrl'
//	})//
//	.when('/users/user-area', {
//		templateUrl : 'views/amh-user-area.html',
//		controller : 'AmhUserAreaCtrl'
//	})//
//	.when('/users/:id', {
//		templateUrl : 'views/amh-user.html',
//		controller : 'AmhUserCtrl'
//	});//
//});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeHelp')

/**
 * @ngdoc controller
 * @name AmhTipsCtrl
 * @description List of all tips
 * 
 * Manages a view with list of all tips.
 * 
 */
.controller('AmhTipsCtrl', function($scope, $help) {
	
	/*
	 * Load tips
	 */
	function _load(){
		$help.tips()
		.then(function(tips){
			ctrl.items = tips.items;
			ctrl.index = -1;
			next();
		});
	}
	
	function previous(){
		ctrl.index -= 1;
		if(ctrl.index < 0){
			ctrl.index = ctrl.items.length -1;
		}
		$scope.tip = ctrl.items[ctrl.index];
	}
	
	function next(){
		ctrl.index = (ctrl.index + 1) % ctrl.items.length;
		$scope.tip = ctrl.items[ctrl.index];
	}
	
	
	var ctrl={};
	$scope.ctrl = ctrl;
	
	$scope.next = next;
	$scope.previous = previous;
	
	_load();
});
///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialHomeHelp')
///**
// * Rewrite default notification of the system
// */
//.run(function($actions) {
//	$actions.newAction({
//		icon: 'info',
//		title: 'Info',
//		description: 'Help center of the application',
////		action: function(){
////			return $navigator.openDialog({
////				templateUrl : 'views/dialogs/amh-tips.html',
////				config : {
////				}
////			});
////		},
//		groups:['amh.owner-toolbar.public']
//	});
//});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
//
//angular.module('ngMaterialHomeHelp')
///**
// * دریچه‌های محاوره‌ای
// */
//.run(function($widget) {
//
//    // Page
//    $widget.newWidget({
//	type: 'PageNotFoundErrorAction',
//	templateUrl : 'views/widgets/amh-pageNotfoundAction.html',
//	label : 'Page not found action',
//	description : 'Actions which are needed in page not found error status.',
//	image : 'images/wb/content.svg',
//	help : 'http://dpq.co.ir/more-information-link',
//	setting: [ 'description', 'border',
//	    'background',
//	    'selfLayout' ],
//	    data : {
//		type : 'PageNotFoundErrorAction',
//		style : {},
//	    }
//    });
//});
angular.module('ngMaterialHomeHelp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/dialogs/amh-tips.html',
    "<md-dialog ng-controller=AmhTipsCtrl ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <wb-icon>info</wb-icon> <h2 translate>Tips and triks</h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding flex> <img ng-src={{tip.image}} height=256px alt={{tip.title}}> <h3>{{tip.title}}</h3> <p>{{tip.description}}</p> <a href={{tip.url}} translate>Read more</a> </md-dialog-content> <md-dialog-actions layout=row> <span flex></span> <md-button ng-click=next() md-autofocus> <span translate>Next</span> </md-button> <md-button ng-click=previous() md-autofocus> <span translate>Previous</span> </md-button> </md-dialog-actions> </md-dialog>"
  );

}]);
